from django.utils.safestring import mark_safe
from django.utils.html import format_html
from django.contrib.staticfiles.templatetags.staticfiles import static
from wagtail.contrib.modeladmin.options import ModelAdmin
from wagtail.core import hooks
import requests

class SyncPanel:
    order = 50

    def render(self):
        return mark_safe("""
        <section class="panel summary nice-padding">
          <a href="#"><button class="button yes">Sync with Datastore</button></a>
        </section>
        """)

@hooks.register('insert_global_admin_css')
def global_admin_css():
    return format_html('<link rel="stylesheet" href="{}">', static('/css/core.css'))

@hooks.register('construct_homepage_panels')
def add_sync_panel(request, panels):
  return panels.append( SyncPanel() )

