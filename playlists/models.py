from django.db import models

from modelcluster.fields import ParentalKey

from wagtail.core.models import Page, Orderable
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.search import index


class NewPlaylist(Page):
    # Database fields
    description = models.CharField(max_length=200)
    date = models.DateField("Post date")

    # Search index configuration

    search_fields = Page.search_fields + [
        index.SearchField('description'),
        index.FilterField('date'),
    ]


    # Editor panels configuration

    content_panels = Page.content_panels + [
        FieldPanel('date'),
        FieldPanel('description', classname="full"),
        InlinePanel('playlist_items', label="Playlist Items"),
    ]

    promote_panels = [
        MultiFieldPanel(Page.promote_panels, "Common page configuration"),
    ]

    parent_page_types = ['wagtailcore.Page']

class PlaylistItem(Orderable):
    page = ParentalKey(NewPlaylist, on_delete=models.CASCADE, related_name='playlist_items')
    media_image = models.ForeignKey(
      'wagtailimages.Image',
      null=True,
      blank=True,
      on_delete=models.SET_NULL,
      related_name='+'
    )

    panels = [
      ImageChooserPanel('media_image'),
    ]

