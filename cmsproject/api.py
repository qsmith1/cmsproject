from wagtail.api.v2.endpoints import PagesAPIEndpoint
from wagtail.api.v2.router import WagtailAPIRouter
from wagtail.images.api.v2.endpoints import ImagesAPIEndpoint
from wagtail.documents.api.v2.endpoints import DocumentsAPIEndpoint
from wagtail.images.models import Image
from wagtail.documents.models import Document

from django.db.models.signals import post_save
from django.db.models.signals import post_delete

import requests
import json

# Create the router. "wagtailapi" is the URL namespace
api_router = WagtailAPIRouter('wagtailapi')

# Add the three endpoints using the "register_endpoint" method.
# The first parameter is the name of the endpoint (eg. pages, images). This
# is used in the URL of the endpoint
# The second parameter is the endpoint class that handles the requests
api_router.register_endpoint('pages', PagesAPIEndpoint)
api_router.register_endpoint('images', ImagesAPIEndpoint)
api_router.register_endpoint('documents', DocumentsAPIEndpoint)

def get_data(sender, **kwargs):
    r = requests.get('http://localhost:8000/api/v2/images')
    rpost = requests.post('http://localhost:3000/test', json=json.loads(r.text))

post_save.connect(get_data, sender=Image)
post_delete.connect(get_data, sender=Image)